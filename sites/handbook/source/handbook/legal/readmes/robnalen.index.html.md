---
layout: markdown_page
title: "Rob Nalen's README"
---

## Rob's README

## Who am I?

My name is Robert Nalen, most people call me Rob (you can call me Rob). Click here to see my [LinkedIn](https://www.linkedin.com/in/rob-nalen-6383a917/)

## What do I do?

I am the Director of Contracts & Legal Operations at GitLab. As titles don’t always provide insight into a person's role, or make any sense at all--looking at you “Wizard of Lightbulb Moments” and “Senior Global Director of Globalness”-- here’s what I do. 
- I am honored to manage a great team of Contract Managers who review and negotiate GitLab’s contracts for both procurement and sales activities. My role as a manager is to assist with escalations, help create playbooks and approval matrices (which they contribute to), carry out any other actions to ensure their success, and make terrible dad jokes to keep positive morale. Side note--just between us--these people are awesome!
- As for Legal Operations: I work with the Contract Managers, and all other legal members, to ensure we have the correct tools and processes to scale with a growing business and remain as efficient as possible. 
- Other items I work on (at random): Drafting agreement templates, negotiating agreements, creating training and enablement materials and videos, answering open source questions, managing the legal tools for all GitLab members, engaging other groups to ensure the legal team is represented and connected where applicable, and anything else people trust me with. 

## Some things to know about me

- I try (key word=try) to bring humor to the team, and anyone I work with. We spend so much of our lives with our co-workers; I love getting to know folks and [where possible] create a friendly and fun atmosphere. 
- My first career was as a school teacher, where I also coached wrestling. 
- Once leaving education, every company I have worked for has been either Open Source or Open Core. 
- GitLab is my second startup, first being Ansible. I’ve come to realize that I really enjoy working at startups. One major element is with startups you are creating the foundation for the company to grow, there is a level of creativity and flexibility that appeals to my problem solving nature. 
- I truly enjoy teaching and enablement. I am always happy to meet with anyone (my calendar is up to date) to talk about items like: Open Source, various business models, how GitLab’s terms work...etc. 
- I am extremely passionate about my work. As a former athlete, it is ingrained in me to provide maximum effort with anything that I do (perhaps to a fault).  


## My working style

- I'm an [INFJ-A](https://www.16personalities.com/profiles/5d75bb9df7f75) with the traits: assertive advocate, diplomat, and confident individualism. 
- I prefer to work asynchronously for work-related tasks and projects, but I love and prefer video calls for non-work: coffee chats, 1:1s, or Friday happy hours.
- I need visual aids and will always ask for them. If a visual aid is unavailable, I prefer our first collaboration focus on creating that artifact to help me understand the topic.
- Work-life balance is important to me and I'm usually completely unavailable after 5pm CST. I do allow email notifications for urgent matters.
- Sometimes I neglect the micro details because I favor the macro ones. I encourage people to tell me when I'm focused on the wrong detail.
- Collaboration is critical to my thought process. I prefer building solutions with others and struggle when I'm left to ideating by myself.
- I have a [low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame) - an acquired skill at GitLab :joy: - which may conflict with others; please stop me and tell me if you feel my shame level is _too_ low.
- My preference is to always "Ship it!" and iterate and improve something later. I have a strong bias for action, but please stop me here as well if it becomes too much.

## How I communicate / the best ways to communicate with me

- If you ever need my immediate attention, reach out via slack: @rnalen 
- Overall, I am attentive and will respond to any Issue, email or SFDC chatter as soon as I can. That being said, I am a very visual person and with certain topics / questions I find having a 15-25 minute zoom conversation can yield way more value than other communication. If I ask for a call, please don’t assume there is an issue or problem. Generally, the call is only going to be helpful and reduce cycles for both myself and the requestor. 
- If you have a question and/or request, it is always best to provide as much insight into the topic. I will always appreciate more information over less, and many times I struggle with how to help a person because I’m unsure of the details surrounding their question(s)/request(s).


## Things about me besides GitLab

- I live in Raleigh, but am originally from Upstate New York
- I have a puppy named “Hamilton” who is a Soft Coated Wheaten Terrier
- I’ve seen the musical “Hamilton”, including with the original cast, three (3) times on two (2) different continents
- I love history and traveling. My favorite place on the planet (so far) is Florence, Italy. 
- I have a daughter who means the world to me. 


